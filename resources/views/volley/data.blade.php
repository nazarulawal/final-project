@extends('layouts.isi')

@section('content')
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Posisi</th>
      <th scope="col">Alamat</th>
      <th scope="col">Pilihan Tempat Main</th>
      <th scope="col">Pilihan Waktu Main</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Arul</td>
      <td>PG</td>
      <td>Cibiru</td>
      <td>Dago</td>
      <td>Pukul 10.00 WIB</td>
      <td>
        <a href="#" class="btn btn-outline-info" >show</a> 
        <a href="#" class="btn btn-outline-warning">edit</a>
        <input type="submit" value="delete" class="btn btn-outline-danger">
      </td>
    </tr>
  </tbody>
</table>
<a href="#" class="btn btn-outline-success" >Tambahkan Teman</a> 

@endsection